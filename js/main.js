$(document).ready(function() {
    $('a[href^="#"]').on('click', function(event) {
        event.preventDefault();
        if($(this).attr('href') != '#') {
            var target = $(this.getAttribute('href'));
            if( target.length) {
                $('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        }
        if($(this).hasClass('hamburger-button')) {
            $('.header-nav ul').toggleClass('expanded');
        }
        return false;
    });

    $('.header-nav ul li a').on('click', function(event) {
        $('.header-nav ul').toggleClass('expanded');
    });
});
